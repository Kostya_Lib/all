﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameInfo Session;
    public Proxy proxy;
    public Proxy2 proxy2;
    private void Start()
    {
        proxy2 = new Proxy2();
        proxy2.CompleteLvl();

        proxy = new Proxy();
        proxy.CompleteLvl();
    }
    public void CompleteLvl()
    {
        Session.CompleteLvl();
    }
}
