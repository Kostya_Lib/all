﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Company : MonoBehaviour
{
    public IWorker worker = new Adaptee(new Builder());

    private void Start()
    {
        worker.Work();
    }
}
