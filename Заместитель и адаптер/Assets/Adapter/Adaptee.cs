﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adaptee : IWorker
{
    public Builder builder;
    public Adaptee( Builder builder)
    {
        this.builder = builder;
    }

    

    public void Work()
    {
        builder.Build();
    }
    public void PayMoney(int v)
    {
        builder.GetSalary(new Salary(v));
    }
}
