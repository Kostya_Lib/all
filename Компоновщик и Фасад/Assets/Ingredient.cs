﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient
{
    public string _name;
    protected float _mass;
    protected float _kal;
    protected float _price;

    public Ingredient(string name,float mass,float kal,float price)
    {
        _name = name;
        _mass = mass;
        _kal = kal;
        _price = price;
    }
    public virtual void Cut()
    {

    }
    public virtual void Boil() 
    {

    }
    public virtual void Fry()
    { 

    }
    public virtual float GetTotalCost() 
    {
        return 0;
    }
    public virtual float GetTotalWeight()
    {
        return 0;
    }
    public virtual float GetTotalCalories()
    {
        return 0;
    }
    public virtual void AddIngredient(Ingredient i) 
    { 

    }
    public virtual void RemoveIngredient(Ingredient i)
    {

    }


}
