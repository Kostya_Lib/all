﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleIngredient : Ingredient
{
    public SimpleIngredient(string name, float mass, float kal, float price) : base(name, mass, kal, price)
    {

    }

    public override void AddIngredient(Ingredient i)
    {
        
    }

    public override void Boil()
    {
        Debug.Log("Варим " + _name);
    }

    public override void Cut()
    {
        Debug.Log("Режим " + _name);
    }

    public override void Fry()
    {
        Debug.Log("Жарим " + _name);
    }

    public override float GetTotalCalories()
    {
        return _kal * _mass;
    }

    public override float GetTotalCost()
    {
        return _mass * _price;
    }

    public override float GetTotalWeight()
    {
        return _mass;
    }

    public override void RemoveIngredient(Ingredient i)
    {
        
    }
}
