﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buter : MonoBehaviour
{
    void Start()
    {
        SimpleIngredient moye = new SimpleIngredient("мазик", 20, 20, 20);
        SimpleIngredient Ketch = new SimpleIngredient("кетчуп", 30, 30, 30);
        ComplexIngredient Ketchanez = new ComplexIngredient("кетчанез",new List<Ingredient> {moye,Ketch});
        SimpleIngredient Bread = new SimpleIngredient("хлэб", 10, 10, 10);
        SimpleIngredient Sir = new SimpleIngredient("сыр", 50, 50, 50);
        ComplexIngredient buter = new ComplexIngredient("KIBG_BUTER", new List<Ingredient> { Bread, Sir, Ketchanez, Bread });

        Debug.Log(buter._name);
        Debug.Log(buter.GetTotalCalories());
        Debug.Log(buter.GetTotalWeight());
        Debug.Log(buter.GetTotalCost());
    }

    
}
