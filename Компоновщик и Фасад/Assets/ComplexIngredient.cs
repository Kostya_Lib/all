﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplexIngredient : Ingredient
{
    
    public ComplexIngredient(string name, List<Ingredient> list) : base(name, 0, 0, 0)
    {
        ingredients = list;
    }
    private List<Ingredient> ingredients;

    public override void AddIngredient(Ingredient i)
    {
        ingredients.Add(i);
    }

    public override void Boil()
    {
        base.Boil();
    }

    public override void Cut()
    {
        base.Cut();
    }

    public override void Fry()
    {
        base.Fry();
    }

    public override float GetTotalCalories()
    {
        float sosi = 0;
        for (int i = 0; i < ingredients.Count; i++)
        {
            sosi += ingredients[i].GetTotalCalories();
        }return sosi;
    }

    public override float GetTotalCost()
    {
        float dl = 0;
        for (int i = 0; i < ingredients.Count; i++)
        {
            dl +=  ingredients[i].GetTotalCost();
        }
        return dl;
    }

    public override float GetTotalWeight()
    {
        float mas = 0;
        for (int i = 0; i < ingredients.Count; i++)
        {
            mas += ingredients[i].GetTotalWeight();
        }
        return mas;
    }

    public override void RemoveIngredient(Ingredient i)
    {
        ingredients.Remove(i);
    }
}
