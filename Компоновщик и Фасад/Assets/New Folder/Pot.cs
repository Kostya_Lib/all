﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pot : MonoBehaviour
{
    public void Boil(Ingredient ingredient,float time)
    {
        ingredient.Boil();
    }

}
