﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public void Cut(Ingredient ingredient)
    {
        ingredient.Cut();
    }
}
