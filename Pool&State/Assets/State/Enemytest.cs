﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemytest : MonoBehaviour
{
    [SerializeField] Transform[] pos;
    [SerializeField] private float speed = 3.5f;
    [SerializeField] private float accuracy = 0.5f;
    int currPoition;

    public int rays;
    public int distance = 15;
    public float angle = 60;
    private Vector3 offset;
    public Transform target;

    public bool See = false;

    private void LateUpdate()
    {
        

        if (currPoition != pos.Length)
        {
            Vector3 lookAtTarget = new Vector3(pos[currPoition].position.x,
                                               this.transform.position.y,
                                               pos[currPoition].position.z);

            //this.transform.LookAt(lookAtTarget);

            if (See == true)
            {
                this.transform.LookAt(target.position);
                transform.Translate(0, 0, speed * Time.deltaTime);
            }
            else
            {
                this.transform.LookAt(lookAtTarget);
                if (Vector3.Distance(this.transform.position, lookAtTarget) > accuracy)
                {
                    
                    transform.Translate(0, 0, speed/2 * Time.deltaTime);
                }
                else
                {
                    currPoition++;
                }
            }

        }
        else
        {
            currPoition = 0;
        }
    }
    private void Update()
    {
        if (Vector3.Distance(transform.position, target.position) < distance)
        {
            if (RayToScan())
                See = true;
        }
        else
        {
            See = false;
        }
    }

    bool GetRaycast(Vector3 dir)
    {
        bool result = false;
        RaycastHit hit = new RaycastHit();
        Vector3 pos = transform.position + offset;
        if (Physics.Raycast(pos, dir, out hit, distance))
        {
            if (hit.transform == target)
            {
                result = true;
                Debug.DrawLine(pos, hit.point, Color.green);
            }
            else
            {
                Debug.DrawLine(pos, hit.point, Color.blue);
            }
        }
        else
        {
            Debug.DrawRay(pos, dir * distance, Color.red);
        }
        return result;
    }

    bool RayToScan()
    {
        bool result = false;
        bool a = false;
        bool b = false;
        float j = 0;
        for (int i = 0; i < rays; i++)
        {
            var x = Mathf.Sin(j);
            var y = Mathf.Cos(j);

            j += angle * Mathf.Deg2Rad / rays;

            Vector3 dir = transform.TransformDirection(new Vector3(x, 0, y));
            if (GetRaycast(dir)) a = true;

            if (x != 0)
            {
                dir = transform.TransformDirection(new Vector3(-x, 0, y));
                if (GetRaycast(dir)) b = true;
            }
        }

        if (a || b) result = true;
        return result;
    }

}
