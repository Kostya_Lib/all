﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [SerializeField] private State _state = null;

    public void StartState(State state)
    {
        if(state != null)
        {
            _state = state;
            state.Enter();
        }
    }

    public void СhangeS(State state)
    {
        if(state != null)
        {
            state.Exit();
            _state = state;
            state.Enter();
        }
    }
}
