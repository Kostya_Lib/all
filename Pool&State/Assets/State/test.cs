﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class test : MonoBehaviour
{
    private NavMeshAgent NMA;
    public Animator animator;
    private Rigidbody rb;

    private void Start()
    {
        NMA = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float speed = NMA.remainingDistance;
        if (speed <= 0.01f)
        {
            animator.SetBool("run", false);
        } else if (speed >= 1)
        {
            animator.SetBool("run", true);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Pos;
            if (Physics.Raycast(ray, out Pos))
            {
                NMA.SetDestination(Pos.point);
            }
   
        }
    }
}
