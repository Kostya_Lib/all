﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private static GameObject enemy;
    private static Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        enemy = this.gameObject;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wall")
        {
            Pool.Instanse.Set("bullet");
            Rig(false, new Vector3(0, 0, 0), 0);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            Pool.Instanse.Set("bullet");
            Rig(false, new Vector3(0, 0, 0), 0);
        }
    }
    public static void Rig(bool get, Vector3 v, int Forse)
    {
        if (get == true)
        {
            rb.isKinematic = false;
            rb.AddForce(0, Forse, 0);
            enemy.transform.position = v;
        }
        else
        {
            rb.isKinematic = true;

        }
    }
}
