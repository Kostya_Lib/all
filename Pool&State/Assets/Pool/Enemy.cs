﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private static GameObject enemy; //вот ты создал что-то
    private static Rigidbody rb; // тут ты сделал статичный рб 

    private void Start()
    {
        rb = GetComponent<Rigidbody>(); // тут ты присвоил рб то что это рб на игроке
        enemy = this.gameObject; // тут ты ему сказал что это этот объект
    }
    public static int EnemyGo;
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wall")
        {
            Pool.Instanse.Set("enemy");
            Rig(false, new Vector3(0, 0, 0), 0);
            EnemyGo++;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bullet")
        {
            Pool.Instanse.Set("enemy");
            Rig(false, new Vector3(0, 0, 0), 0);
        }
    }

    public static void Rig(bool get, Vector3 v, int Forse)
    {
        if (get == true) 
        {
           rb.isKinematic = false;
           rb.AddForce(0, Forse, 0);
           enemy.transform.position = v;
        }
        else 
        {
            rb.isKinematic = true;
            
        }
    }
}
