﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PerseptronTestData 
{
    public string name;
    public List<int> inputValues = new List<int>();
    public int outputValue;
}

public class Perceptron : MonoBehaviour
{
    public List<PerseptronTestData> testData = new List<PerseptronTestData>();
    public PerseptronTestData pts;

    private void Start()
    {
        Random rng = new Random();
        double[] weight = { Random.Range(-1, 2), Random.Range(-1, 2) };
        double bias = Random.Range(-1, 2);
        
        double error = 0;
        
        while (error > 0.2)
        {
             error = 0;
            for (int i = 0; i < 4; i++)
            {
                int output = CalculaterOutput(testData[i].inputValues[i], testData[i].inputValues[i], bias, weight);

                int localerror = testData[i].outputValue - output;

                weight[0] = testData[i].inputValues[i] * error + weight[0];
                weight[1] = testData[i].inputValues[i] * error + weight[1];

            }
        }
    }

    private static int CalculaterOutput(double input1,double input2,double bias,double[] weight)
    {
        return 0;
    }
}
