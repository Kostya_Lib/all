﻿using UnityEditor;
using Malee.List;

[CustomEditor(typeof(Perceptron))]
public class PerceptronEditor : Editor
{
    private ReorderableList list1;

    private void OnEnable()
    {
        list1 = new ReorderableList(serializedObject.FindProperty("testData"));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        list1.DoLayoutList();

        serializedObject.ApplyModifiedProperties();
    }
}
