﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : IStrategy
{
    public float Speed;


    public MoveForward(float speed)
    {
        Speed = speed;
    }

    public void Perform(Transform tr)
    {
        tr.position += new Vector3(0, Speed * Time.deltaTime, 0);
    }
}
