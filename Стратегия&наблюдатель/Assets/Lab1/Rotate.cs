﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : IStrategy
{
    public float Speed;


    public Rotate(float speed)
    {
        Speed = speed;
    }
    public void Perform(Transform tr)
    {
        tr.Rotate(0f, Speed * Time.deltaTime, 0f);
    }
}
