﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emmit : IStrategy
{
    public int Value;


    public Emmit(int speed)
    {
        Value = speed;
    }

    public void Perform(Transform tr)
    {
        tr.GetComponent<ParticleSystem>().Emit(Value);
    }
}
