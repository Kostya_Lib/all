﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Performer : MonoBehaviour
{
    public IStrategy srt;

   public void SetStrategy(IStrategy strNew)
   {
        srt = strNew;
   }
    private void Update()
    {
        srt?.Perform(this.transform);
    }
    public void SetStrategy(int SteNamber)
    {
        switch (SteNamber)
        {
            case 1:
                SetStrategy(new MoveForward(10));
                break;
            case 2:
                SetStrategy(new Rotate(45));
                break;
            case 3:
                SetStrategy(new Emmit(10));
                break;
            case 4:
                SetStrategy(new MoveForward(-10));
                break;
            case 5:
                SetStrategy(new Rotate(-45));
                break;
            case 6:
                SetStrategy(new Emmit(20));
                break;
        }
    }
}
