﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceExchanger : MonoBehaviour
{
    void Start()
    {
        ResourceBank.Instanse.OnResChanged += UP;//подписывание
    }

    private void UP(ResourceBank.ResType obj)
    {
        if(ResourceBank.Instanse.GetResource(ResourceBank.ResType.Дерево) >= 10)
            
        {
            ResourceBank.Instanse.ChangeResource/*изменение реса*/(ResourceBank.ResType.Дерево/*тип ресурса*/, -10/*то на сколько*/);
            ResourceBank.Instanse.ChangeResource(ResourceBank.ResType.Дом, 1);
        }

        if (ResourceBank.Instanse.GetResource(ResourceBank.ResType.Дом) >= 5)
        {
            Debug.Log("';s'a");
            int r = UnityEngine.Random.Range(1, 5);
            ResourceBank.Instanse.ChangeResource(ResourceBank.ResType.Дом, -r);
            ResourceBank.Instanse.ChangeResource(ResourceBank.ResType.Деньги, r*100);
        }

        if (ResourceBank.Instanse.GetResource(ResourceBank.ResType.Деньги) >= 1000)
        {
            int r = UnityEngine.Random.Range(1, 4);
            ResourceBank.Instanse.ChangeResource(ResourceBank.ResType.Деньги, -(r * 250));
            ResourceBank.Instanse.ChangeResource(ResourceBank.ResType.Рабочий, r);
            
        }
    }
}
