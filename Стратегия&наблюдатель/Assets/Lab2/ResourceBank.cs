﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ResourceBank : MonoBehaviour
{

    IDictionary<ResType, int> Resurses = new Dictionary<ResType, int>  //создание словаря
    {
        { ResType.Рабочий,2}, // заполнение сразу с значениями
        { ResType.Дерево,0}, // заполнение сразу с значениями
        { ResType.Дом,0}, // заполнение сразу с значениями
        { ResType.Деньги,0} // заполнение сразу с значениями
    };
    public Action<ResType> OnResChanged; //хз как именно работает Action но через него мы подписываемся на изменения значений

    public enum ResType //типы ресурсов
     {
            Рабочий,
            Дерево,
            Дом,
            Деньги
     };
    public static ResourceBank Instanse; //переменная для синглтона

    private void Awake() //проверка для синглтона
    {
        if(Instanse == null)
        {
            Instanse = this;
        }
        else
        {
            Destroy(this);
        }
        
    }

    private void Start()
    {
        StartCoroutine(OnChangedWood()); //запуск корутины
    }
    public void ChangeResource(ResType res, int val)
    {
        Resurses[res] += val;
        OnResChanged(res);
    }

    public int GetResource(ResType res)
    {
        return Resurses[res];
    }
    IEnumerator OnChangedWood()
    {
        while (true)
        {
            Debug.Log("кек");
            ChangeResource(ResType.Дерево, GetResource(ResType.Рабочий));
            yield return new WaitForSeconds(0.5f);
        }

    }
}
