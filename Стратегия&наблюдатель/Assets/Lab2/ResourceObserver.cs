﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceObserver : MonoBehaviour
{
    public ResourceBank.ResType resT; //этим ты будешь выбирать какой ресурс отображать
    private Text textTree; // создание текста
    private void Start()
    {
        textTree = GetComponent<Text>(); // тут он берёт текст с объекта где код но ты можешь сделать текст публичным и руками написать 
        ResourceBank.Instanse.OnResChanged/* тут ты подписался на изменение значний ресурсов*/ += Newtext; // просто название метода,пиши любое и он через ошибку предложет создать
    }

    private void Newtext(ResourceBank.ResType obj) // как раз метод который он создал
    {
        if (resT == obj)  // тут мы сравнием тип если выбранный нами ресурс равен тому же ресурсу который получаем
        {
            textTree.text = ResourceBank.Instanse.GetResource(resT).ToString(); //то тогда текст принимает значение количества этого ресурса и через ToString мы его переводим в читаемое
        };
    }
}
