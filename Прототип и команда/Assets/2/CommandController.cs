﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandController : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            CommandInvoker.Instence.AddCommand(new MoveForward(transform, 1));
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            CommandInvoker.Instence.AddCommand(new Rotate(transform,90));
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            CommandInvoker.Instence.AddCommand(new Rotate(transform, -90));
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            CommandInvoker.Instence.ProcessAll();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CommandInvoker.Instence.Process();
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            CommandInvoker.Instence.Undo();
        }

    }
}
