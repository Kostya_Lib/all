﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandInvoker : MonoBehaviour
{
    public static CommandInvoker Instence;
    Queue<ICommand> commands = new Queue<ICommand>();
    Stack<ICommand> commandsTrue = new Stack<ICommand>();

    private void Awake()
    {
        if (Instence == null)
        {
            Instence = this;
        }
        else
        {
            Destroy(this);
        }

    }
    public void AddCommand(ICommand command)
    {
        commands.Enqueue(command);
    }
    public void ProcessAll() 
    {
            while (commands.Count >= 1)
            {
                ICommand command;
                command = commands.Dequeue();
                commandsTrue.Push(command);
                command.Invoke();
            }
    }
    public void Process() 
    {
        if (commands != null && commands.Count >= 1)
        {
            ICommand command;
            command = commands.Dequeue();
            commandsTrue.Push(command);
            command.Invoke();
        }
    }
    public void Undo()
    {
        if (commands != null)
        {
            ICommand command;
            command = commandsTrue.Pop();
            command.Undo();
        }
    }
    public void ClearQueue() 
    {
        if (commands != null)
        {
            commands.Clear();
        }
    }
}
