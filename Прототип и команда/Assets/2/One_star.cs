﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface ICommand
{
    void Invoke();
    void Undo();
    string Description { get;  }
}
 

public class MoveForward : ICommand
{
    public Transform t;
    public float dist;

    public MoveForward(Transform t, float dist)
    {
        this.t = t;
        this.dist = dist;
    }

    public string Description { get => "Двигаем что-то там " + t + " " + dist; }

    public void Invoke()
    {
        t.Translate(t.forward * dist);
    }

    public void Undo()
    {
        t.Translate(t.forward / dist);
    }
}

public class Rotate : ICommand
{
    public Transform t;
    public float angle;

    public Rotate(Transform t, float angle)
    {
        this.t = t;
        this.angle = angle;
    }

    public string Description => "поворачивает " + t + " " + angle;


    public void Invoke()
    {
        Debug.Log(angle);
        t.RotateAroundLocal(t.up , angle);
        //поворачивает transform t на angle
    }


    public void Undo()
    {
        t.RotateAroundLocal(t.up, -angle);
    }
}
