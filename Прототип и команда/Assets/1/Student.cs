﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Student : ICloneable
{
    public string name;
    public string last_name;
    public int age;
    public Group group;

    public Student(string name, string last_name, int age, Group group)
    {
        this.name = name;
        this.last_name = last_name;
        this.age = age;
        this.group = group;
    }

    public object Clone()
    {
        return new Student(name,last_name,age,new Group(group.course,group.spec));
    }
}
